var app = angular.module('innoengines')

app.factory('Currentuser', function ($resource){
	return $resource('/api/account/:id', { id: '@_id'},{
		'update' : { method: 'PUT' }
	}); 
});

app.factory('Innovation', function ($resource){
	return $resource('/api/newinnovation/:id', { id: '@_id'},{
		'update' : { method: 'PUT' }
	}); 
});