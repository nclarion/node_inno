'use strict';

var app = angular.module('innoengines');

app.controller('AdminCtrl', function($scope, $modal, $log, $alert, Innovation, $rootScope, $route, $routeParams, DbCollection, Restangular, $upload, $resource, $http, $q, $filter, ngTableParams, $location){
	
	$scope.row = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];

	$scope.fieldType =[
	{
		'type' : 'files', 'value' : 1
	},
		{
		'type' : 'number', 'value' : 2
	},
		{
		'type' : 'text_area', 'value' : 3
	},
		{
		'type' : 'option', 'value' : 4
	},
		{
		'type' : 'date', 'value' : 5
	},
		{
		'type' : 'text', 'value' : 8
	},
		{
		'type' : 'password', 'value' : 9
	},
		{
		'type' : 'email', 'value' : 10
	}
	];

	$scope.column = [1,2,3,4];

	$scope.statusfields = [
	{'type' : 'active', 'value' : 1},
	{'type' : 'Archived', 'value' : 2},
	{'type' : 'Inactive', 'value' : 3}
	]

	$scope.statustabs = [
	{'type' : 'active', 'value' : 1},
	{'type' : 'Archived', 'value' : 2},
	{'type' : 'Inactive', 'value' : 3}
	]

	$http.get(DbCollection + '/api/getbricklist/')
	.then(function(result){
		$rootScope.dataresultlist = result.data;
	})

	$http.get(DbCollection + '/api/fields/')
	.then(function(result){
		$rootScope.datafields = result.data;
	})

	$scope.savebrick = function(){

			$http.post(DbCollection + '/api/getbricklist/', $scope.brick)
			.success(function(data){
				$scope.dataresult = data[0];
		
		        $scope.dataresultlist.push($scope.dataresult);

		        $scope.brick.name = "";
		        $scope.brick.notes = "";


			});

	};


	$scope.updatebrick = function(id){

		$http.put(DbCollection + '/api/gebricklist/'+ id)
		.success(function(data){
			$scope.dataresult = data[0];
			console.log($scope.dataresult);
		});

	};

	$scope.deletebrick = function(index, id){

		$http.delete(DbCollection + '/api/getbricklist/'+ id);
		$scope.dataresultlist.splice(index, 1)[0];

	};


	$scope.savefields = function(){

			$http.post(DbCollection + '/api/fields/', $scope.field)
			.success(function(data){
				$scope.datafieldsresult = data[0];
		
		        $scope.datafields.push($scope.datafieldsresult);

		        $scope.field.name = "";
		        $scope.field.notes = "";
		
			});

	};


	$scope.deletefield = function(index, id){

		$http.delete(DbCollection + '/api/fields/'+ id);
		$scope.datafields.splice(index, 1)[0];

	};

	$rootScope.editmode = 'false';

     $scope.editfield = function(id){
  
        $rootScope.editmode = 'true';

        $http.get(DbCollection + '/api/fields/'+ id)
        .then(function(result){
        $rootScope.accountfield = result.data;
     
        });
    }

    $rootScope.editmode = 'tabfalse';

     $scope.editTab = function(id){
  
        $rootScope.editmode = 'tabtrue';
        $http.get(DbCollection + '/api/getbricklist/'+ id)
        .then(function(result){
        $rootScope.accountab= result.data;
        console.log(result.data);
     
        });
    } 

    $scope.updatefield = function(id, index){
  
        $rootScope.editmode = 'false';
        console.log(index);
		$http.put(DbCollection + '/api/fields/'+ id, $scope.accountfield)
		.success(function(data){
			$scope.updateresult = data[0];
			window.location.reload();
		});
    } 

    $scope.updateTab = function(id, index){
  
        $rootScope.editmode = 'tabfalse';
        console.log(index);
		$http.put(DbCollection + '/api/getbricklist/'+ id, $scope.accountab)
		.success(function(data){
			$scope.updateresult = data[0];
			window.location.reload();
		});
    } 



});