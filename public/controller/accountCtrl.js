'use strict';

var app = angular.module('innoengines');

app.controller('accountCtrl', function($scope, $rootScope,  $route, DbCollection, Restangular, $upload, $resource, $http, $q, $filter, ngTableParams, $location){

$rootScope.innohead = [
 {list : 'Account list'},
 {form : 'Account Form'}
];

  $scope.onFileSelect = function($files) {
    //$files: an array of files selected, each file has name, size, and type.
      for (var i = 0; i < $files.length; i++) {
      var file = $files[i];
   
      $scope.upload = $upload.upload({
        url: DbCollection +'/users/imageupload', //upload.php script, node.js route, or servlet url
        method: 'POST', 
        //'POST' or 'PUT',
        //headers: {'header-key': 'header-value'},
        //withCredentials: true,
        data: {myObj: $scope.myModelObj},
        file: file, // or list of files ($files) for html5 only
        //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
        // customize file formData name ('Content-Disposition'), server side file variable name. 
        //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file' 
        // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
        //formDataAppender: function(formData, key, val){}
      }).progress(function(evt) {
        console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
      }).success(function(data, status, headers, config) {
        // file is uploaded successfully
        console.log(data);
        console.log(file);

      });

      //.error(...)
      //.then(success, error, progress); 
      // access or attach event listeners to the underlying XMLHttpRequest.
      //.xhr(function(xhr){xhr.upload.addEventListener(...)})
    }
    /* alternative way of uploading, send the file binary with the file's content-type.
       Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed. 
       It could also be used to monitor the progress of a normal http post/put request with large data*/
    // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.

  };


  $rootScope.toggle = 'show';
  $rootScope.submitcaption = 'Edit Mode'

  $scope.edit = function(id){
    $rootScope.editmode = 'show';
    $rootScope.submitcaption = 'Update Account';

    $http.get(DbCollection + '/users/accountname/' + id)
      .then(function(result){
      $scope.edituser = result.data;
    });

  }

  $scope.delete = function(id) {

        $http.delete(DbCollection + '/users/editaccount/' + id);
        window.location.reload();
  };

  $scope.updateaccount = function(id){

       $http.put(DbCollection + '/users/editaccount/' + id, $scope.account);
       $rootScope.editmode = 'hide';
       $rootScope.submitcaption = 'Edit Mode';
       $location.path('/');
        window.location.reload();
  }

  $scope.reload = function(){
  }
  
  $scope.updateuser = function(id, accountnname){
       $rootScope.editusers = 'false';
       $rootScope.successupdate = 'true';
       $rootScope.useraccount.accountname_owner = accountnname; 
       $http.put(DbCollection + '/users/register/' + id, $scope.useraccount);
       $location.path('/');
      
  }

  $rootScope.editusers = 'false';

  $scope.edituser = function(id, accountnname){
      $rootScope.editusers = 'true';
      $http.get(DbCollection + '/users/accountdata/'+ id)
      .then(function(result){
      $rootScope.useraccount = result.data;
      $rootScope.useraccount.accountname = accountnname; 
      
    
      });
  } 

  $scope.cancel = function(){
       $rootScope.editmode = 'hide';
       $rootScope.submitcaption = 'Edit Mode';

  }

  $scope.navClass = function (page) {
    var currentRoute = $location.path().substring(1) || 'home';
    return page === currentRoute ? 'active' : '';
  };

  $http.get(DbCollection + '/users/accountname/')
      .then(function(result){
      $scope.account_result = result.data;

  });

  $http.get(DbCollection + '/users/accountdata/')
      .then(function(result){
      $rootScope.userlist = result.data;

  });

  $scope.accountname_result = Restangular.all("accountname").getList().$object;

  $scope.show = function(id){
    
  $http.get(DbCollection + '/users/accountname/'+ id)
      .then(function(result){
      $rootScope.account = result.data;
      $rootScope.createaccountget = result.data;
    
    });

  };


  $scope.save = function(){

    $http.post(DbCollection + '/users/register/', $scope.createaccountget);
    console.log($scope.createaccountget.pswrd);
    $location.path('/');

   }
$scope.imagepath = DbCollection + '/uploads/';
  });

app.controller('AdduserCtrl', function($scope, Account, $rootScope, DbCollection, Restangular, $resource, $http, $q, $filter, ngTableParams, $location){
$scope.imagepath = DbCollection + '/uploads/';
$scope.account = Account;

});