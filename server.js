var express 	= require('express'),
    mongodb     = require('mongoskin'),
    bodyParser  = require('body-parser'),
    methodOverride  = require('method-override');
   	path		= require('path'),
	config 		= require("./config"),
    ejs         = require('ejs'),
	api 		= require('./api'),
	users 		= require('./account'),
	uploader 	= require('./uploadManager'),
    upload      = require('./upload'),
    resizeConf  = require('./configupload').resizeVersion,
    dirs        = require('./configupload').directors,
    swig        = require('swig'),
    db          = mongodb.db(config.mongodb.inno1, {native_parser:true}),
	app 		= express();
ejs.open = '{{';
ejs.close = '}}';
app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'ejs');
app.use(bodyParser());
app.use(methodOverride());
app.engine('.html', require('ejs').__express);
// Optional since express defaults to CWD/views

app.set('views', __dirname + '/public');

// Without this you would need to
// supply the extension to res.render()
// ex: res.render('users.html').
app.set('view engine', 'html');
app.use(express.static(path.join(__dirname, '/uploads')));
app.use(express.static(path.join(__dirname + '/public')));
// app.use(express.static(path.join(__dirname + '/public/views')));
app.use(express.static(__dirname + '/public/uploaded/files'));
app.use(users);
app.use(api);
app.use(uploader);
app.use('/api', api);
app.use('/users', users);
app.use('/uploader', uploader);

app.get('*', function (req, res){

    if(req.role === 'adminAccessControl'){
        res.sendfile('public/create_account.html');
    }else if(req.access === 'Admin'){
        res.sendfile('public/main.html');
    }else if(req.access === 'Editor'){
        res.sendfile('public/main.html');
    }else if(req.access === 'Viewer'){
        res.sendfile('public/main.html');
    }else{
        res.redirect('/account');
    }

});

app.get('/getform', function (req, res){
      res.sendfile('public/form.html');
});

// jquery-file-upload helper
app.use('/upload/default', function (req, res, next) {
    upload.fileHandler({
        tmpDir: dirs.temp,
        uploadDir: __dirname + dirs.default,
        uploadUrl: dirs.default_url,
        imageVersions: resizeConf.default
    })(req, res, next);
});

app.use('/upload/location', upload.fileHandler({
    tmpDir: dirs.temp,
    uploadDir: __dirname + dirs.location,
    uploadUrl: dirs.location_url,
    imageVersions: resizeConf.location
}));

app.use('/upload/location/list', function (req, res, next) {
    upload.fileManager({

        uploadDir: function () {
            return __dirname + dirs.location;
            console.log(__dirname + dirs.location);
        },
        uploadUrl: function () {
            return dirs.location_url;
            console.log(dirs.location_url);
        }

    }).getFiles(function (files) {
        res.json(files);
        console.log(files);
    });
});

// bind event
upload.on('end', function (fileInfo) {
    // insert file info
    console.log("files upload complete");
    console.log(fileInfo);
});

upload.on('delete', function (fileName) {
    // remove file info
    console.log("files remove complete");
    console.log(fileName);
});

upload.on('error', function (e) {
    console.log(e.message);
});



app.get('/render', function (req, res) {
        res.sendfile('/public/form.html');

        // var html = [
        //     '<p>Call this url in browser : http://localhost:3001/location/input <a href="/location/input">Go</a></p>',
        //     '<p>Call this url in browser : http://localhost:3001/upload/location/list <a href="/upload/location/list">Go</a></p>'
        // ].join('');
        // res.send(html);

 });

app.get('/location/input',function (req, res) {
        var params = {
            title: "jquery file upload example"
        };

        res.send('form', params);

});

app.post('/location/input', function (req, res) {
        var image_info = req.body;
            image_info.usercurrentId = req.user;
        db.collection('imageinfo').insert(image_info, function (err, data){
            res.json(data);
        });
});




app.listen(config.server.port);
 console.log(config.message);
 console.log('Database URL running "' + config.mongodb.inno1 + '..."');
 console.log("Server listening on port " + config.server.port);
