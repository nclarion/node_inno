var express 	= require('express'),
	config 		= require("./config"),
	bodyparser 	= require('body-parser'),
	session 	= require('express-session'),
	bourne 		= require('bourne'),
	cors 		= require('cors'),
	mongodb 	= require('mongoskin'),
	crypto 		= require('crypto'),
	http 		= require('http'),
	formidable 	= require('formidable'),
	path 		= require('path'),
	qt 			= require('quickthumb'),
	multipart   = require('connect-multiparty'),
	util		= require('util'),
	fs			= require('fs-extra'),
	db 			= mongodb.db(config.mongodb.inno1, {native_parser:true}); 

var router = express.Router();


function hash (password) {
	return crypto.createHash('sha256').update(password).digest('hex');
}

router
.use(cors())
.use(bodyparser.urlencoded())
.use(bodyparser.json())
.use(session({secret  : 'sdfsdf46fgfhy45fffed5jkfdfszzvm890ffdhdj',
			  maxAge  : new Date(Date.now() + 3600000),
              expires : new Date(Date.now() + 3600000) })) //will expire in 1 hour

.get('/account', function (req, res){
	res.sendfile('public/account_authentication.html');
})
.get('/accountdata', function (req, res){
	db.collection('users').find().toArray(function (err, data){
		res.json(data);
	})
})
.get('/accountdata/:id', function (req, res){
	db.collection('users').findById(req.params.id, function (err, data){
			res.json(data);
	})
})
.post('/accountaccess', function (req, res){
	var account = {
		accountname : req.body.accountname
	};
	db.collection('accountname').findOne(account, function (err, data){
		if (data){

			req.session.accountId 		= data._id;
			req.session.adminrole 		= data.role;
			req.session.accountname 	= data.accountname;
			console.log(req.session.accountname);

			res.sendfile('public/login.html');

		}else{
			res.redirect('/account');
		}
	})
})
.post('/login', function (req, res){

	var user = {
		username : req.body.username,
		password : hash(req.body.password),
		accountname : req.session.accountname
	};
	              	
		console.log(user.username);
	db.collection('users').findOne(user, function (err, data){
		if (data){
			req.session.user_id 		= data._id;
			req.session.role 			= data.role;
			req.session.access 			= data.roles;
			req.session.username 		= data.username;
			console.log(data.username);
			res.redirect('/');
			
		}else{
			res.redirect('/login');
			// res.send(401, 'User name does not exist');

		}
	})
});

router
.use(cors())
.use(bodyparser.urlencoded())
.use(bodyparser.json())
.post('/register', function (req, res){
        var today = new Date();
		var user = {

		username 	: req.body.username,
		pswrd 		: req.body.password,
		password 	: hash(req.body.password),
		role     	: req.body.role,
		roles     	: req.body.roles,
		active    	: req.body.active,
		accountname : req.body.accountname,
		refferId 	: req.session.user_id,
		byEmail  	: req.session.username,
		created 	: today

	};


	db.collection('users').find({username: user.username}).toArray(function (err, data){
		if(!data.length){
			db.collection('users').insert(user, function(err, data){
			        res.redirect('/');
			});
		}else{
			res.redirect('/login');
			console.log('Error: Sorry, cannot create account "'+ user.username +'": Username exists');

		}
	
	})
})
.put('/register/:id', function (req, res, next){

		var today = new Date();

		var user = {
		username : req.body.username,
		pswrd : req.body.pswrd,
		password : hash(req.body.pswrd),
		roles     : req.body.roles,
		active    : req.body.active,
		accountname  : req.body.accountname,
		refferId : req.session.user_id,
		byEmail  : req.session.username,
		created : today

	};
		
		db.collection('users').updateById(req.params.id, user, function (err, data){
			res.json(data);
		
		});
})

router
.use(cors())
.use(bodyparser.urlencoded({limit : '1000mb'}))
.use(bodyparser.json({limit : '1000mb'}))
.post('/createaccountname', function (req, res){

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();


		var account = {

		accountname : req.body.accountname,
		description : req.body.accountDescription,
		role : req.body.role,
		created: today,
		active : 'Active'

		};


	db.collection('accountname').find({accountname: account.accountname}).toArray(function (err, data){
		if(!data.length){
			db.collection('accountname').insert(account, function(err, data){
					req.session.accountnameId = data._id;
					req.session.accountname = data.accountname;
					res.redirect('/');
			});
		}else{
			res.redirect('/login');
			console.log('Error: Sorry, cannot create account "'+ user.accountname +'": account name exists');

		}
	
	})
})
.get('/accountname', function (req, res){
		db.collection('accountname').find().toArray(function (err, data){
			res.json(data);
					
		});
})
.get('/accountname/:id', function (req, res){
		db.collection('accountname').findById(req.params.id, function (err, data){
			res.json(data);
		});
});

router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '1000mb'}))
    .use(bodyparser.json({limit: '1000mb'}))
	.route('/editaccount/:id')
	.get(function (req, res){
		db.collection('accountname').findById(req.params.id, function (err, data){
			res.json(data);
		});
	})
	.put(function (req, res, next){

		var today = new Date();

		var account = {

			accountname : req.body.accountname,
		    description : req.body.description,
		    created 	: today,
		    avatar 		: req.body.avatar,
		    role		: req.body.role,
		    active		: req.body.active

		}
		
		db.collection('accountname').updateById(req.params.id, account, function (err, data){
			res.json(data);

		});
	})
	.delete(function (req, res){
		db.collection('accountname').removeById(req.params.id, function(){
			res.json(null);
		});
	});
	
router
    .use(cors())
    .use(bodyparser.urlencoded({limit: '1000mb'}))
    .use(bodyparser.json({limit: '1000mb'}))

.post('/upload', function (req, res, next) {

    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        // `file` is the name of the <input> field of type `file`
        var old_path = files.file.path,
        	avatar = files.file.name,
            file_size = files.file.size,
            file_ext = files.file.name.split('.').pop(),
            index = old_path.lastIndexOf('/') + 1,
            file_name = old_path.substr(index),
            // new_path = path.join(process.env.PWD, '/uploads/', file_name + '.' + file_ext);
            new_path = path.join(process.env.PWD, '/uploads/', avatar);
 			

 			console.log(avatar);

        fs.readFile(old_path, function(err, data) {
            fs.writeFile(new_path, data, function(err) {
                fs.unlink(old_path, function(err) {
                    if (err) {
                        res.status(500);
                        res.json({'success': false});
                    } else {
                        res.status(200);
                        res.json({'success': true});
                    }
                });
            });
        });
    });
})

// Use quickthumb
.use(qt.static(__dirname + '/'))

.post('/imageupload', function (req, res, multipart){
  var form = new formidable.IncomingForm();
  var files = [];
  var fields = [];
  form.parse(req, function(err, fields, files) {
    
    res.writeHead(200, {'content-type': 'text/plain'});
    res.write('received upload:\n\n');
    res.end(util.inspect({fields: fields, files: files}));
  });

  form.on('end', function(fields, files) {
    /* Temporary location of our uploaded file */
    var temp_path = this.openedFiles[0].path;
    /* The file name of the uploaded file */
    var file_name = this.openedFiles[0].name;
    /* Location where we want to copy the uploaded file */
    var new_location = 'uploads/';

    fs.copy(temp_path, new_location + file_name, function(err) {  
      if (err) {
        console.error(err);
      } else {
        console.log("success!")
      }
    });
  });
})

.get('/logout', function (req, res){

		req.session.user_id  	= 	null;
	    req.session.username 		= 	null;
		req.session.role  			= 	null;
	    req.session.accountId  		= 	null;
		req.session.accountname  	= 	null;
	    req.session.adminrole  		= 	null;
	    req.session.access  		= 	null;

	    res.redirect('/account');

})

.use(function (req, res, next){
	
	if(req.session.user_id){

			req.user_id 		= 		req.session.user_id;
			req.byusername 		= 		req.session.username;
			req.role 			= 		req.session.role;
			req.accountId		= 		req.session.accountId;
			req.accountname 	= 		req.session.accountname;
			req.adminrole 		= 		req.session.adminrole;
			req.access          = 		req.session.access;


	}

	next();
	
});


module.exports = router;